(require '[aoc2020.aoc :as aoc]
         '[clojure.string :as string])

(def example-input (aoc/get-input "7-example"))

(defn parse-input [i]
  (let [[bag contents] (string/split i #" bags contain ")
        other-bags (for [b (string/split contents #" bags?[,.]")]
                     (string/trim b))
        parsed-bags (for [b other-bags
                          :let [m (re-matches #"(\d+) (.*?)" b)]]
                      (if m
                        {(last m) (Integer/parseInt (second m))}))]

    {bag (into {} parsed-bags)}))

(defn map-rules [i]
  (let [parsed-input (map parse-input i)]
    (into {} parsed-input)))

(defn can-contain-bag? [bag target rules]
  (let [contents (get rules bag)
        has-bag (contains? contents target)
        other-bags (keys contents)]
    (cond has-bag true
          (some? other-bags) (some true? (for [b other-bags]
                                           (can-contain-bag? b target rules)))
          :else false)))

(defn has-how-many-bags? [bag rules]
  (let [contents (get rules bag)
        bags (apply + (vals contents))]
    (if (empty? contents) 1
        (apply + (cons 1 (for [[k v] (seq contents)]
                             (* (has-how-many-bags? k rules) v)))))))

(defn challenge7a []
  (let [input (aoc/get-input 7)
        rules (map-rules input)
        bags (keys rules)
        can-hold-gold (map #(can-contain-bag? % "shiny gold" rules) bags)]
    (->> can-hold-gold
         (filter true?)
         count)))

(defn challenge7b []
  (let [input (aoc/get-input 7)
        rules (map-rules input)
        total-bags (has-how-many-bags? "shiny gold" rules)]
    (- total-bags 1)))
