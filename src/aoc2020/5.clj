(require '[aoc2020.aoc :as aoc]
         '[clojure.string :as cstr])

(defn input-to-binary-string [i]
  (cstr/replace i #"L|R|F|B" {"R" "1" "B" "1" "L" "0" "F" "0"}))

(defn parsed-input-to-row-col [p]
  (let [row (take 7 p)
        col (drop 7 p)]
    [(Integer/parseInt (apply str row) 2)
     (Integer/parseInt (apply str col) 2)]))

(defn get-seat-id [v]
  (let [[row col] v
        x (* row 8)]
    (+ x col)))

(defn check-seat [i]
  (let [b (input-to-binary-string i)
        v (parsed-input-to-row-col b)]
    (get-seat-id v)))

(assert (= (check-seat "BFFFBBFRRR") 567))
(assert (= (check-seat "FFFBBBFRRR") 119))
(assert (= (check-seat "BBFFBBFRLL") 820))

(defn challenge5a []
  (let [input (aoc/get-input 5)
        ids (map check-seat input)]
    (apply max ids)))

(defn challenge5b []
  (let [input (aoc/get-input 5)
        ids (map check-seat input)
        sorted (sort ids)]
    (loop [i sorted]
      (let [f (first i)
            s (second i)]
        (cond (= s (+ 2 f)) (+ 1 f)
              :else (recur (rest i)))))))
