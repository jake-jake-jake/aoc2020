(require '[aoc2020.aoc :as aoc])

(def example-input (aoc/get-input "10-example"))

(def example-input-2 (aoc/get-input "10-example-2"))

(def count-configs
  (memoize
   (fn
     ([x] 1)
     ([x y & r]
      (if (> (+ x y) 3)
        (apply count-configs y r)
        (+ (apply count-configs y r)
           (apply count-configs (+ x y) r)))))))

(defn deltas [i]
  (let [ints (for [l i] (Integer/parseInt l))]
    (->> ints
         (apply max)
         (+ 3)
         (conj ints 0)
         sort
         (partition 2 1)
         (mapv (fn [[a b]] (- b a))))))

(defn challenge10a []
  (let [input (aoc/get-input 10)
        ds (deltas input)
        {ones 1 threes 3} (frequencies ds)]
    (* ones threes)))

(defn challenge10b []
  (->> (aoc/get-input 10)
       deltas
       (apply count-configs)))
