(require '[aoc2020.aoc :as aoc])

(def input (aoc/get-input 6))

(defn make-sets [input]
  (loop [i input
         sets []
         s #{}]
    (let [f (first i)]
      (cond (nil? f) (conj sets s)
            (= f "") (recur (rest i)
                            (conj sets s)
                            #{})
            :else (recur (rest i)
                         sets
                         (into s f))))))

(defn compare-answers [common this-group]
        (clojure.set/intersection common this-group))

(defn get-common-answers [answers]
  (reduce compare-answers (for [answ answers
                                         :let [s (into #{} answ)]]
                                     s)))

(defn challenge6a []
  (let [input (aoc/get-input 6)
        yes-answers (make-sets input)]
    (apply + (map #(count %) yes-answers))))
(challenge6a)

(defn challenge6b []
  (let [input (aoc/get-input 6 #"\n\n")
        answer-groups (for [ch input
                            :let [s (clojure.string/trim ch)]]
                        (clojure.string/split-lines s))
        common-answers (for [group answer-groups]
                         (get-common-answers group))]
    (apply + (map #(count %) common-answers))))
(challenge6b)
