(require '[aoc2020.aoc :as aoc])

(def example-input (aoc/get-input "11-example"))

(defn coords-around [x y]
  (for [x1 [(dec x) x (inc x)]
        y1 [(dec y) y (inc y)]
        :when (not (and (= x1 x) (= y1 y)))]
    [x1 y1]))

(def dirs
  (for [x [-1 0 1]
        y [-1 0 1]
        :when (not (and (= x 0) (= y 0)))]
    [x y]))
(defn get-neighbors [[x y] v]
  (let [coords (coords-around x y)]
    (for [[x y] coords]
      (-> v (get y) (get x)))))

(defn see [[x y] [dx dy] v]
  (loop [distance 1]
    (let [nx (+ x (* distance dx))
          ny (+ y (* distance dy))
          seeing (-> v (get ny) (get nx))]
      (if (= seeing \.)
        (recur (inc distance))
        seeing))))

(defn look-around [[x y] v]
  (for [dir dirs]
        (see [x y] dir v)))

(defn process-state [current seen]
  (let [fs (frequencies seen)
        ]
    (cond (and (= current \L)
               (= (get fs \# 0)
                  0 ))
          \#
          (and (= current \#)
               (>= (get fs \# 0)
                  5 ))
          \L
          :else current)))

(defn make-seat-vector [i]
  (into [] (for [l i]
             (into [] l))))

(defn next-state [[x y] v]
  (let [current-state (-> v (get y) (get x))

        seen (look-around [x y] v)
        ]
    (process-state current-state seen )))

(defn walk-vector [v]
  (for [y (range (count v))
        x (range (count (get v 0)))]
    [x y]))

(defn do-step [v]
  (let [y-size (count v)
        x-size (count (get v 0 0))
        next-cells (for [cell (walk-vector v)]
                     (next-state cell v))]
    (into [] (for [l (partition x-size next-cells)]
               (into [] l)))))
(defn count-occupied [v]
  (->> (flatten v) (filter #(= \# %)) count))

(defn challenge11b []
  (let [input (aoc/get-input 11)
        seats (make-seat-vector input)]
    (loop [last-seats seats
           previous-seat-count 0
           reps 0]
      (let [new-seats (do-step last-seats)
            last-seats-count (count-occupied last-seats)
            new-seats-count (count-occupied new-seats)]
        (if (= (mod reps 50) 0) (println "rep num:" reps))
        (cond (< 10000 reps) "infinite loop"
              (= last-seats-count new-seats-count previous-seat-count) [(count-occupied new-seats) reps]
              :else (recur new-seats last-seats-count (inc reps)))))))
