(ns aoc2020.aoc
  (:gen-class))

(require 'clojure.string)

;; load each line of an input file
(defn get-input [n & split-on]
  (let [contents (slurp (str  "inputs/", n))]
    (println split-on)
    (if (nil? split-on)
      (clojure.string/split-lines contents)
      (clojure.string/split contents (first split-on)))))
