(require '[aoc2020.aoc :as aoc])

(defn check-num [n s target]
  (let [t (- target n)]
    (if (contains? s t)
      [true s]
      [false (conj s n)])))


(defn find-pair [nums seen target]
  (let [f (first nums)
        r (rest nums)
        [is-num new-seen] (check-num f seen target)]
    (cond (true? is-num) [f (- target f)]
          (nil? r) nil
          :else (recur r new-seen target))))

(defn make-summed [num seen]
  (into {} (for [n seen]
             {(+ num n) [n num]})))

(defn find-triple [nums summed seen]
  (let [f (first nums)
        r (rest nums)
        target (- 2020 f)]
    (cond (contains? summed target) (conj (get summed target) f)
          (nil? rest) nil
          :else (recur r (conj summed (make-summed f seen)) (conj seen f)))))

(defn challenge1a []
  (let [input (map #(Integer/parseInt %) (aoc/get-input 1))
        seen #{}
        [a b] (find-pair input seen 2020)]
    (* a b)))

(defn challenge1b []
  (let [input (map #(Integer/parseInt %) (aoc/get-input 1))
        first-two (into [] (take 2 input))
        first-two-added (reduce + first-two)
        summed {first-two-added first-two}
        rest (drop 2 input)
        [a b c] (find-triple rest summed (into #{} first-two))]
    (* a b c)))
