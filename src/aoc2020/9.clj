(require '[aoc2020.aoc :as aoc])

(def example-input (aoc/get-input "9-example"))

(defn is-good-num? [preamble n]
  (let [f (first preamble)
        r (rest preamble)
        s (into #{} r)
        d (- n f)]
    (cond (contains? s d) true
          (empty? r) false
          :else (recur r n))))

(defn check-chunks [v preamble-length]
  (let [pre (take preamble-length v)
        n (->> v (drop preamble-length) first)]
    (cond (nil? n) (do (println "No bad num found") nil)
          (not (is-good-num? pre n)) n
          :else (recur (rest v) preamble-length))))

(defn find-sum-set [v target]
  (loop [s-idx 0
         a-idx 1
         acc (get v s-idx)]
    (let [a-num (get v a-idx)
          n-acc (+ acc a-num)]
      (cond (= n-acc target) [s-idx a-idx]
            (> n-acc target) (recur (inc s-idx) (inc (inc s-idx)) 0)
            :else (recur s-idx (inc a-idx) n-acc)))))

(defn challenge9a []
  (let [input (aoc/get-input 9)
        nums (for [i input]
               (Integer/parseInt i))]
    (check-chunks nums 25)))

(defn challenge9b []
  (let [input (aoc/get-input 9)
        nums (into [] (for [i input]
                        (bigint i)))
        bad-num (check-chunks nums 25)
        [start end] (find-sum-set nums bad-num)
        range-nums (subvec nums start end)
        biggest (apply max range-nums)
        smallest (apply min range-nums)]
    (+ biggest smallest)))
