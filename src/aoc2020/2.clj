(require '[aoc2020.aoc :as aoc])

(def example-input
  ["1-3 a: abcde"
   "1-3 b: cdefg"
   "2-9 c: ccccccccc"])

(defn parse-input [i]
  (re-matches #"(\d+)-(\d+) ([a-z]): ([a-z]+)" i))

(defn check-password [w]
  (if-let [m (parse-input w)]
    (let [[_ low high c pass] m
          num-c (->> (re-seq (re-pattern c) pass) count)
          in-range (and (>= num-c (Integer/parseInt low))
                        (<= num-c (Integer/parseInt high)))]
      in-range)
    false))
(assert (= (->> (filter check-password example-input) count) 2))

(defn check-password-2 [w]
  (if-let [m (parse-input w)]
    (let [[_ low high c pass] m
          [idx1 idx2] [(- (Integer/parseInt low) 1) (- (Integer/parseInt high) 1)]
          idx1-matches (= (nth pass idx1) (first c))
          idx2-matches (= (nth pass idx2) (first c))
          one (or idx1-matches idx2-matches)
          not-both (not (and idx1-matches idx2-matches))
          valid (and one not-both)]
      valid)
    false))
(assert (= (->> (filter check-password-2 example-input) count) 1))

(defn challenge2a []
  (let [input (aoc/get-input 2)
        num-good (->> (filter check-password input)
                      count)]
    num-good))
(challenge2a)
(defn challenge2b []
  (let [input (aoc/get-input 2)
        num-good (->> (filter check-password-2 input)
                      count)]
    num-good))
(challenge2b)
