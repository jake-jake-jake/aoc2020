(require '[aoc2020.aoc :as aoc])

;; (def input (aoc/get-input 3))

(defn make-grid [i]
  (into [] (for [l i]
             (into [] l))))

;; (def grid (make-grid input))

(defn glide [sx sy grid & {:keys [dx dy] :or {dx 3 dy 1}}]
  (for [move (range (count grid))
        :let [y (+ sy (* move dy))
              line (nth grid y)
              x (mod (+ sx (* move dx))
                     (count line))]
        :while (< (+ y dy) (count grid))]
    [x y]))
;; (glide 0 0 grid)

(defn get-char [coords grid]
  (let [[x y] coords
        line (nth grid y)
        c (nth line x)]
    c))

(defn get-path [sx sy grid & {:keys [dx dy] :or {dx 3 dy 1}}]
  (let [coords (glide sx sy grid :dx dx :dy dy)]
    (for [coord coords]
      (get-char coord grid))))

(defn count-trees [path]
  (->> path (filter #(= \# %)) count))

(defn check-trajectory [dx dy grid]
  (let [path (get-path 0 0 grid :dx dx :dy dy)]
    (count-trees path)))

(defn challenge3a []
  (let [grid (make-grid (aoc/get-input 3))]
    (check-trajectory 3 1 grid)))
(challenge3a)

(defn challenge3b []
  (let [grid (make-grid (aoc/get-input 3))
        a (check-trajectory 1 1 grid)
        b (check-trajectory 3 1 grid)
        c (check-trajectory 5 1 grid)
        d (check-trajectory 7 1 grid)
        e (check-trajectory 1 2 grid)]
    (* a b c d e)))
(challenge3b)
