(require '[aoc2020.aoc :as aoc]
         '[clojure.string :as string])

(def example-input (aoc/get-input "8-example"))

(defn parse-input [i]
  (let [[cmd n] (string/split i #" ")]
    [cmd (Integer/parseInt n)]))

(defn do-command [c acc idx]
  (let [[cmd n] c]
    (cond (nil? cmd) [acc idx]
          (= cmd "nop") [acc (inc idx)]
          (= cmd "acc") [(+ acc n) (inc idx)]
          :else [acc (+ idx n)])))

(defn run-program [input]
  (let [cmds (for [i input]
               (parse-input i))
        prog (into [] cmds)]
    (loop [acc 0
           idx 0
           seen #{0}]
      (let [cmd (get prog idx)
            [n-acc n-idx] (do-command cmd acc idx)]
        (cond (nil? cmd) acc
              (contains? seen n-idx) acc
              :else (recur n-acc n-idx (conj seen n-idx)))))))

(defn swap-cmd [c]
  (let [[cmd n] c
        n-cmd (if (= "nop" cmd) "jmp" "nop")]
    [n-cmd n]))

(defn run-til-failure [prog]
  (loop [acc 0
         idx 0
         seen #{}]
    (let [cmd (get prog idx)
          [n-acc n-idx] (do-command cmd acc idx)]
      (cond (nil? cmd) acc
            (contains? seen n-idx) nil
            :else (recur n-acc n-idx (conj seen n-idx))))))

(defn fix-program [input]
  (let [cmds (for [i input]
               (parse-input i))
        prog (into [] cmds)]
    (loop [idx 0]
      (let [cmd (get prog idx)
            s-cmd (swap-cmd cmd)
            s-prog (assoc prog idx s-cmd)]
        (cond (nil? cmd) "Unable to fix..."
              (some? (run-til-failure prog)) (run-til-failure prog)
              (some? (run-til-failure s-prog)) (run-til-failure s-prog)
              :else (recur (inc idx)))))))

(defn challenge8a []
  (let [input (aoc/get-input 8)]
    (run-program input)))

(defn challenge8b []
  (let [input (aoc/get-input 8)]
    (fix-program input)))
