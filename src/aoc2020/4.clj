(require '[aoc2020.aoc :as aoc])

(def example-input (aoc/get-input "4-example"))

(defn parse-input [l]
  (re-seq #"([a-z]+):(\S+)" (or l "")))

(defn make-map [m]
  (into {} (for [[_ k v] m]
             [k v])))

(defn read-passports [i]
  (loop [input i
         passports []
         passport {}]
    (let [l (first input)
          r (rest input)
          m (parse-input l)]
      (cond (nil? l) (conj passports passport)
            (nil? m) (recur r
                            (conj passports passport)
                            {})
            :else (recur r
                         passports
                         (conj passport (make-map m)))))))

(defn in-range [lower higher n]
  (and (<= lower n)
       (>= higher n)))

(defn valid-birth-year [y]
  (if y
    (let [m (re-find #"\d{4}" y)
          n (Integer/parseInt (or m "0"))]
      (in-range 1920 2002 n))
    false))

(defn valid-issue-year [y]
  (if y
    (let [m (re-find #"\d{4}" y)
          n (Integer/parseInt (or m "0"))]
      (in-range 2010 2020 n))
    false))

(defn valid-expiration-year [y]
  (if y
    (let [m (re-find #"\d{4}" y)
          n (Integer/parseInt (or m "0"))]
      (in-range 2020 2030 n))
    false))

(defn check-height [v u]
  (cond (= u "cm") (in-range 150 193 v)
        (= u "in") (in-range 59 76 v)
        :else false))

(defn valid-height [h]
  (if h
    (let [m (re-matches #"(\d+)(cm|in)" h)
          [_ v u] (or m [])
          n (Integer/parseInt (or v "0"))]
      (check-height n u))))

(defn valid-hair-color [c]
  (if c
    (let [m (re-matches #"#[a-f0-9]{6}" c)]
      (some? m))))

(defn valid-eye-color [e]
  (if e
    (let [m (re-matches #"^(amb|blu|brn|gry|grn|hzl|oth)$" e)]
      (some? m))))

(defn valid-pass-num [p]
  (if p
    (let [m (re-matches #"^\d{9}$" p)]
      (some? m))))

(defn validate-passport [p]
  (let [required ["ecl" "pid" "eyr" "hcl" "byr" "iyr" "hgt"]
        has-req (every? true? (for [k required]
                                (contains? p k)))
        valid-byr (valid-birth-year (get p "byr"))
        valid-iyr (valid-issue-year (get p "iyr"))
        valid-eyr (valid-expiration-year (get p "eyr"))
        valid-hgt (valid-height (get p "hgt"))
        valid-hcl (valid-hair-color (get p "hcl"))
        valid-ecl (valid-eye-color (get p "ecl"))
        valid-pid (valid-pass-num (get p "pid"))]
    (and has-req valid-byr valid-iyr valid-eyr valid-hgt valid-hcl valid-ecl valid-pid)))

(def passports (read-passports example-input))

(->> passports
     (filter #(validate-passport %))
     count)

(defn challenge4a []
  (let [input (aoc/get-input 4)
        passports (read-passports input)]
    (->> passports
         (filter #(validate-passport %))
         count)))
(challenge4a)
